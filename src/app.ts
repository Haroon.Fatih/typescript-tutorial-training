import { Invoice } from './classes/invoice.js';
import { Payment } from './classes/Payment.js';
import { ListTemplate } from './classes/ListTemplate.js';
import { HasFormatter } from './interfaces/HasFormatter.js';

const form = document.querySelector('.new-item-form') as HTMLFormElement;

// inputs
const type = document.querySelector('#type') as HTMLInputElement;
const tofrom = document.querySelector('#tofrom') as HTMLInputElement;
const details = document.querySelector('#details') as HTMLInputElement;
const amount = document.querySelector('#amount') as HTMLInputElement;

// list template instance
const ul = document.querySelector('ul')!;
const list = new ListTemplate(ul);

form.addEventListener('submit', (e: Event) => {
  e.preventDefault();

  // Below is a tuple array, this allows us to spread the values in lines
  // here new Invoice(...values); and here Payment(...values);
  // they are spread by the value... bit
  // This ensures that the types of variables passed through the function match
  let values: [string, string, number];
  values = [tofrom.value, details.value, amount.valueAsNumber];

  let doc: HasFormatter;
  if (type.value === 'invoice') {
    doc = new Invoice(...values);
  } else {
    doc = new Payment(...values);
  }
  
  list.render(doc, type.value, 'end');
});

// tuples
// type of data is fixed in each position once it has been initialised

// note that with arrays we can have any tyoe initially chosen in the array 
// for instance in teh array below we have a number, string and a boolean 
// this means that we can have these types in any order in an array as below:
let arr = ['ryu', 25, true];
arr[0] =  true;
arr[1] = 'yoshi'
arr = [30, false, 'yoshi']

// tuples however can onlyb have the same type in the same index once initialised

let tup: [string, number, boolean] = ['ryu', 20, false];

// This is not ok:
// tup[1] = 'Larry';

// This will not work because we have a number in the first position:
// let tup0: [string, number, boolean] = [30, 20, false];
// tup0[0] = false;

//This is ok:
tup[0] = 'Larry';
tup[1] = 40;

let student: [string, number];
student = [ 'chun-li', 3141 ];

// This will not work as the types do not match:
// student = [ 3141,  'chun-li' ];