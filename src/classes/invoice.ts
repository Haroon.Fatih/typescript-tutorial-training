// import an interface
import { HasFormatter } from '../interfaces/HasFormatter.js'

// to use this class elsewhere we need to add the word export before the class

// In the interface and classes lesson we have added in implements HasFormatter to ensure that the class
// follows the HasFormatter interface which (by the way we define the method) means that it must have the format method
export class Invoice implements HasFormatter {
    // note when we create the fields below we can make them public (this is the default behaviour)
    // We can make it private or readonly the words added to the front of the field are public (or nothing)
    //  private or readonly respectively. 
    // Note that I am delibrtstley choosing public so that the other code below works properly
    public client: string;
    public details: string;
    public amount: number;

    // Note that we can not only specify the above 
    // in addition we need to specify the constructor as well as shown below.

    constructor(c: string, d: string, a: number){
        this.client = c;
        this.details = d;
        this.amount = a;
    }

    format() {
        return `${this.client} owes ${this.amount} for ${this.details} `
    }
}