// to use this class elsewhere we need to add the word export before the class
// In the interface and classes lesson we have added in implements HasFormatter to ensure that the class
// follows the HasFormatter interface which (by the way we define the method) means that it must have the format method
export class Invoice {
    // Note that we can not only specify the above 
    // in addition we need to specify the constructor as well as shown below.
    constructor(c, d, a) {
        this.client = c;
        this.details = d;
        this.amount = a;
    }
    format() {
        return `${this.client} owes ${this.amount} for ${this.details} `;
    }
}
